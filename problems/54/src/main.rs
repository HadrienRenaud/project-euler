use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::{BufRead, BufReader};

use crate::Hand::{
    Flush, FourOfAKind, FullHouse, HighCard, OnePair, Straight, StraightFlush, ThreeOfAKind,
    TwoPairs,
};

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Suit {
    Heart,
    Diamond,
    Spade,
    Club,
}

impl From<&str> for Suit {
    fn from(e: &str) -> Self {
        match e {
            "H" => Self::Heart,
            "D" => Self::Diamond,
            "S" => Self::Spade,
            "C" => Self::Club,
            _ => panic!("Unknown format for suit: {}", e),
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct Card {
    color: Suit,
    number: u8,
}

fn parse_card_number(s: &str) -> u8 {
    match s {
        "2" => 2,
        "3" => 3,
        "4" => 4,
        "5" => 5,
        "6" => 6,
        "7" => 7,
        "8" => 8,
        "9" => 9,
        "T" => 10,
        "J" => 11,
        "Q" => 12,
        "K" => 13,
        "A" => 14,
        _ => panic!("Unknown format for card number: '{}'", s),
    }
}

impl From<&str> for Card {
    fn from(e: &str) -> Self {
        if e.len() < 2 {
            panic!("Not enough characters to parse a card in '{}'", e);
        }

        Card {
            number: parse_card_number(&e[0..1]),
            color: e[1..2].into(),
        }
    }
}

fn parse_uncolored_hands(cards: Vec<u8>) -> Hand {
    let mut forth: Option<u8> = None;
    let mut third: Option<u8> = None;
    let mut pairs: Option<(u8, u8)> = None;
    let mut pair: Option<u8> = None;
    let mut alones: Vec<u8> = Vec::with_capacity(5);

    for c in cards {
        if alones.contains(&c) {
            alones.retain(|&i| i != c);
            match pair {
                None => pair = Some(c),
                Some(d) => pairs = Some(if c < d { (d, c) } else { (c, d) }),
            }
        } else if third == Some(c) {
            forth = Some(c);
        } else if pair == Some(c) {
            third = Some(c);
            pair = None;
        } else {
            match pairs {
                Some((g, o)) | Some((o, g)) if g == c => {
                    pair = Some(o);
                    third = Some(c);
                    pairs = None;
                }
                _ => alones.push(c),
            }
        }
    }

    let alones = alones;
    let forth = forth;
    let third = third;
    let pair = pair;
    let pairs = pairs;

    if forth.is_some() {
        FourOfAKind(forth.unwrap(), alones[0])
    } else if third.is_some() {
        if pair.is_some() {
            FullHouse(third.unwrap(), pair.unwrap())
        } else {
            ThreeOfAKind(third.unwrap(), alones[0], alones[1])
        }
    } else if pairs.is_some() {
        let (a, b) = pairs.unwrap();
        TwoPairs(a, b, alones[0])
    } else if pair.is_some() {
        OnePair(pair.unwrap(), alones[0], alones[1], alones[2])
    } else {
        HighCard(alones[0], alones[1], alones[2], alones[3], alones[4])
    }
}

#[derive(Copy, Clone, Debug, PartialOrd, Eq, PartialEq)]
enum Hand {
    // Five cards parsed from most important to less important
    HighCard(u8, u8, u8, u8, u8),
    // The first element is repeated
    OnePair(u8, u8, u8, u8),
    // The two first elements are repeated
    TwoPairs(u8, u8, u8),
    // The first element is repeated twice
    ThreeOfAKind(u8, u8, u8),
    // A straight beginning at this element
    Straight(u8),
    // A flush with these cards
    Flush(u8, u8, u8, u8, u8),
    // A full, 3 times the first and twice the second
    FullHouse(u8, u8),
    // 4 times the first and the second
    FourOfAKind(u8, u8),
    // A straight flush beginning at this element
    // A Royal Flush is just a straight flush
    StraightFlush(u8),
}

impl fmt::Display for Hand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            HighCard(a, _, _, _, _) => write!(f, "Highest card {}", a),
            OnePair(a, _, _, _) => write!(f, "Pair of {}", a),
            TwoPairs(a, b, _) => write!(f, "Two pairs {} and {}", a, b),
            ThreeOfAKind(a, _, _) => write!(f, "Three {}", a),
            Straight(a) => write!(f, "Straight from {}", a),
            Flush(a, _, _, _, _) => write!(f, "Flush with highest card {}", a),
            FullHouse(a, b) => write!(f, "Full of {} by {}", a, b),
            FourOfAKind(a, _) => write!(f, "Four {}", a),
            StraightFlush(a) => write!(f, "Straight Flush from {}", a),
        }
    }
}

impl From<&str> for Hand {
    fn from(s: &str) -> Self {
        let mut cards: Vec<Card> = s.split_ascii_whitespace().map(|e| Card::from(e)).collect();
        if cards.len() != 5 {
            panic!("There should be 5 cards in this hand: '{}'", s);
        }

        cards.sort_by_key(|&card| -(card.number as i16));

        let (a, b, c, d, e) = match cards[..] {
            [a, b, c, d, e, ..] => (a, b, c, d, e),
            _ => unreachable!(),
        };

        let same_suit =
            a.color == b.color && a.color == c.color && a.color == d.color && a.color == e.color;
        let straight = a.number == b.number + 1
            && a.number == c.number + 2
            && a.number == d.number + 3
            && a.number == e.number + 4;

        if same_suit {
            return if straight {
                StraightFlush(e.number)
            } else {
                Flush(a.number, b.number, c.number, d.number, e.number)
            };
        } else if straight {
            return Straight(e.number);
        }

        parse_uncolored_hands(cards.iter().map(|card| card.number).collect())
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let f = BufReader::new(File::open("./54.txt")?);
    let mut result = 0;

    for line in f.lines() {
        let s = line?;
        // println!("Line {} gave :", s);
        let h1 = Hand::from(&s[..15]);
        // println!("\t- {}", h1);
        let h2 = Hand::from(&s[15..]);
        // println!("\t- {}", h2);

        if h1 > h2 {
            result += 1;
            // println!("And h1 > h2");
        }
        // println!("\n");
    }

    println!("There are {} winning hands for player 1.", result);

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::Hand;

    #[test]
    fn test_compare() {
        let a = Hand::StraightFlush(10);
        let b = Hand::Straight(6);
        let c = Hand::Straight(5);

        assert!(a > b);
        assert!(b > c);
    }

    #[test]
    fn example_of_the_subject() {
        let a = Hand::from("5H 5C 6S 7S KD");
        let b = Hand::from("2C 3S 8S 8D TD");
        assert!(a < b);
        assert!(!(a > b));

        let a = Hand::from("5D 8C 9S JS AC");
        let b = Hand::from("2C 5C 7D 8S QH");
        assert!(a > b);
        assert!(!(a < b));

        let a = Hand::from("2D 9C AS AH AC");
        let b = Hand::from("3D 6D 7D TD QD");
        assert!(a < b);
        assert!(!(a > b));

        let a = Hand::from("4D 6S 9H QH QC");
        let b = Hand::from("3D 6D 7H QD QS");
        assert!(a > b);
        assert!(!(a < b));

        let a = Hand::from("2H 2D 4C 4D 4S");
        let b = Hand::from("3C 3D 3S 9S 9D");
        assert!(a > b);
        assert!(!(a < b));
    }
}
